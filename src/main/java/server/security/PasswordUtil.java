package server.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordUtil {

    private static PasswordEncoder encoder = new BCryptPasswordEncoder();

    public static String encryptPassword(String input) {
        return encoder.encode(input);
    }

    public static boolean checkPassword(String plainPass, String encryptedPass) {
        return encoder.matches(plainPass, encryptedPass);
    }

}
