package server.message.response;


public class JwtResponse {
    private String access_token;
    private String refresh_token;
    private String type = "Bearer";
    private int expires_in;

    public JwtResponse(String accessToken, int expires, String refreshToken) {
        this.access_token = accessToken;
        this.expires_in = expires;
        this.refresh_token = refreshToken;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}