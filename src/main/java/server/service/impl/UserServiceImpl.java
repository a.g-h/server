
package server.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.domain.User;
import server.repository.UserRepository;
import server.security.PasswordUtil;
import server.service.interfaces.UserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void updateUser(User user) {
        user.setId(userRepository.findUserByUsername(user.getUsername()).getId());
        user.setPassword(PasswordUtil.encryptPassword(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public boolean deleteUser(String username) {
        User user = userRepository.findUserByUsername(username);
        if (user!=null){
            userRepository.delete(user);
            return true;
        }
        return false;
    }

    @Override
    public void addUser(User user) {
        user.setPassword(PasswordUtil.encryptPassword(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public String getUserPassword(String username) {
        User user = userRepository.findUserByUsername(username);
        if (user != null)
            return user.getPassword();
        return null;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }
}
