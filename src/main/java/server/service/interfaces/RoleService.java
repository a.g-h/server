package server.service.interfaces;

import server.domain.Role;
import server.domain.RoleName;

public interface RoleService {
    Role findRoleByName(RoleName roleName);
}
