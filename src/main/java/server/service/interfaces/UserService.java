package server.service.interfaces;

import server.domain.User;

import java.util.List;

public interface UserService {
    void updateUser(User user);

    boolean deleteUser(String username);

    void addUser(User user);

    User findUserByUsername(String username);

    String getUserPassword(String username);

    List<User> getAllUsers();
}
