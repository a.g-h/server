package server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import server.domain.Role;
import server.domain.RoleName;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

    Role findRoleByName(RoleName roleName);

}
