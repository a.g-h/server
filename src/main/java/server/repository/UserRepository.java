package server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import server.domain.User;


@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    User findUserByUsername(String username);
}
