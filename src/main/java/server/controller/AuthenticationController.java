package server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import server.domain.Role;
import server.domain.RoleName;
import server.domain.User;
import server.message.request.AuthenticationForm;
import server.security.jwt.JwtProvider;
import server.service.interfaces.RoleService;
import server.service.interfaces.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

@RestController
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;

    private final UserService userService;

    private final RoleService roleService;

    private final JwtProvider jwtProvider;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, UserService userService, RoleService roleService, JwtProvider jwtProvider) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.roleService = roleService;
        this.jwtProvider = jwtProvider;
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody AuthenticationForm loginRequest) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getUsername(),
                            loginRequest.getPassword()
                    ));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            return ResponseEntity.ok(jwtProvider.generateTokens(authentication));

        }catch (BadCredentialsException e){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/registration")
    public ResponseEntity registerUser(@Valid @RequestBody AuthenticationForm signUpRequest) {

        if (userService.findUserByUsername(signUpRequest.getUsername()) != null) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

        User user = new User();
        user.setUsername(signUpRequest.getUsername());
        user.setPassword(signUpRequest.getPassword());

        Set<Role> roles = new HashSet<>();
        Role userRole = roleService.findRoleByName(RoleName.ROLE_USER);
        roles.add(userRole);

        user.setRoles(roles);
        userService.addUser(user);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/username")
    public ResponseEntity getUsername(HttpServletRequest request) {
        Enumeration<String> headers = request.getHeaders("Authorization");
        while (headers.hasMoreElements()) {
            String value = headers.nextElement();
            if ((value.startsWith("Bearer"))) {
                String username = jwtProvider.getUserNameFromJwtToken(value.replace("Bearer ", ""));
                if (username != null)
                    return ResponseEntity.ok(username);
            }
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/refresh")
    public ResponseEntity refreshToken(HttpServletRequest request) {
        Enumeration<String> headers = request.getHeaders("Authorization");
        while (headers.hasMoreElements()) {
            String value = headers.nextElement();
            if ((value.startsWith("Bearer"))) {
                String refresh_token = value.replace("Bearer ", "");
                if (jwtProvider.validateJwtToken(refresh_token)) {
                    return ResponseEntity.ok(jwtProvider.generateTokens(SecurityContextHolder.getContext().getAuthentication()));
                }
            }
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
