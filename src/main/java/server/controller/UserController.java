
package server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import server.domain.Role;
import server.domain.RoleName;
import server.domain.User;
import server.service.interfaces.RoleService;
import server.service.interfaces.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class UserController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private final UserService userService;
    private final RoleService roleService;


    @Autowired
    public UserController(UserService service, RoleService roleService) {
        this.userService = service;
        this.roleService = roleService;
    }

    @PutMapping("/users")
    public ResponseEntity updateUser(@RequestBody User user) {
        try {
            Set<Role> roles = new HashSet<>();
            Role userRole = roleService.findRoleByName(RoleName.ROLE_USER);
            roles.add(userRole);

            user.setRoles(roles);
            userService.updateUser(user);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("User update failed: " + e.getMessage());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/users/delete/{username}")
    @Transactional
    public ResponseEntity deleteUser(@PathVariable String username){
        if (userService.deleteUser(username))
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/users/{username}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> getUserPassword(@PathVariable String username) {
        if (userService.getUserPassword(username) != null)
            return new ResponseEntity<>(userService.getUserPassword(username), HttpStatus.OK);
        return new ResponseEntity<>("Couldn't get user password for encryption!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('USER')")
    @Transactional
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.getAllUsers();
        if (users.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/users/names")
    public ResponseEntity<List<String>> getAllUsernames() {
        List<String> usernames = userService.getAllUsers().stream().map(User::getUsername).collect(Collectors.toList());
        if (usernames.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(usernames, HttpStatus.OK);
    }
}

