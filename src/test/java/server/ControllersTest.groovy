package server

import org.json.JSONObject
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.*
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.testng.Assert
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import server.domain.User

import static groovy.json.JsonOutput.toJson
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ControllersTest extends AbstractTestNGSpringContextTests{

    @Autowired
    private WebApplicationContext context

    @Autowired
    protected TestRestTemplate restTemplate
    private MockMvc mockMvc
    private String refresh_token
    private String access_token

    private PasswordEncoder encoder


    @BeforeClass
    void init(){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build()

        encoder = new BCryptPasswordEncoder()

    }

    @Test
    void getAllUsernameTest(){
        mockMvc.perform(get("/users/names")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
    }

    @Test
    void registrationTest(){

        mockMvc.perform(post("/registration")
                .with(httpBasic("testUser","testuserpass"))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new User(username: 'testUser', password: 'testuserpass'))))
                .andExpect(status().isCreated())
    }

    @Test()
    void loginWithWrongCredentialsTest(){
        mockMvc.perform(post("/login")
                .with(httpBasic("unexistinuser","nosuchpass"))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new User(username: 'unexistinguser', password: 'nosuchpass'))))
                .andExpect(status().isUnauthorized())
    }

    @Test
    void loginTest(){
        MvcResult result = mockMvc.perform(post("/login")
                .with(httpBasic("newuser","123456789"))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new User(username: 'newuser', password: '123456789'))))
                .andExpect(status().isOk())
        .andReturn()

        JSONObject object  = new JSONObject(result.getResponse().getContentAsString())
        refresh_token = object.getString("refresh_token")
        access_token = object.getString("access_token")
    }

    @Test(dependsOnMethods = "loginTest")
    void getUserPasswordTest(){
        MvcResult result = mockMvc.perform(get("/users/" + "newuser")
                            .header("Authorization", "Bearer " + access_token)
                            .accept(MediaType.APPLICATION_JSON))
                            .andExpect(status().isOk())
                            .andReturn()

        Assert.assertTrue(encoder.matches("123456789", result.getResponse().getContentAsString()))
    }

    @Test
    void updateUserTest(){
        mockMvc.perform(put("/users")
                .header("Authorization", "Bearer " + access_token)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new User(username: 'testUser', password: 'testusernewpass'))))
        .andExpect(status().isOk())
    }

    @Test
    void accessTokenExpiredTest(){
        //random token to check if server returns HttpStatus 401
        def headers = new HttpHeaders()
        headers.set("Authorization","Bearer " +  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuZXd1c2VyIiwiaWF0IjoxNTQxNzUyMjIyLCJleHAiOjE1NDE3NTIyODJ9.680LcrxgqVVDw8yQIa7NRbhQgk-ykE9ouRrNcuRezX9Py0Q56ifIk2UT2CNtFJ9TDb11vBPUogwgMBSFV27aVe");
        ResponseEntity<String> response = restTemplate.exchange("/users/" + "newuser", HttpMethod.GET, new HttpEntity<>(headers), String.class)
        Assert.assertEquals(response.getStatusCode(), HttpStatus.UNAUTHORIZED)
    }

    @Test
    void refreshTokenTest(){
        mockMvc.perform(get("/refresh")
            .header("Authorization",  "Bearer " + refresh_token)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
    }

    @Test(dependsOnMethods = "registrationTest")
    void deleteUserTest(){
        mockMvc.perform(delete("/users/delete" + "/testUser")
                .header("Authorization","Bearer " +access_token)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
    }
}
