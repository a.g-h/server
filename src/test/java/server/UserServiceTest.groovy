package server

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests
import org.testng.Assert
import org.testng.annotations.Test
import server.domain.User
import server.service.interfaces.UserService

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserServiceTest extends AbstractTestNGSpringContextTests{

    @Autowired
    private UserService userService

    @Test
    void registerNewUser(){
        def user = new User(username: 'testuser', password: 'testuserpassword')
        userService.addUser(user)
        Assert.assertEquals(userService.findUserByUsername("testuser"),user)
    }

    @Test
    void findUserByUsernameTest(){
        def user = userService.findUserByUsername("myuser")
        Assert.assertEquals(user.username, "myuser")
    }

    @Test
    void deleteUser(){
        def user = new User(username: 'userForDelete', password: 'testuserpass')
        userService.addUser(user)
        Assert.assertTrue(userService.deleteUser("userForDelete"))
    }

    @Test
    void updateUser(){
        def user = new User(username: 'testuser', password: 'testusernewpassword')
        userService.updateUser(user)
        Assert.assertNotEquals(userService.getUserPassword('testuser'),'$2a$10$IlXfopR2w.qbZQ6p1/K4jOdNrzOEdS0uT.AbolcZVnl9B58nBHcsW')
    }

    @Test
    void deleteUpdatedUser(){
        Assert.assertTrue(userService.deleteUser("testuser"))
    }
}
